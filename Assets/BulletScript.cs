﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public GameObject target;
    float created;

    // Start is called before the first frame update
    void Start()
    {
        created = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        float t = Time.time;
        if (t - created > 4)
        {

            Destroy(this.gameObject);

        }
        try
        { 
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, 0.5f);
        }
        catch
        {
            Destroy(this.gameObject);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.gameObject.tag == "Enemies")
        {
            collision.collider.gameObject.GetComponent<EnemyController>().health -= 40;
            Destroy(this.gameObject);
        }

    }

}
