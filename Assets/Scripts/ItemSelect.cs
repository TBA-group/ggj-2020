﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSelect : MonoBehaviour
{
    public bool selected = false;
    public float last_selected = 0;

    public string type = "Plant";
    public GameObject abs_parent;

    bool mouse_over = false;
    bool mouse_clicked = false;
    SpriteRenderer sprite_renderer;
    ItemManager item_manager;

    

    // Start is called before the first frame update
    void Start()
    {
        sprite_renderer = GetComponentInParent<SpriteRenderer>();

        if(sprite_renderer == null)
        {
            sprite_renderer = abs_parent.GetComponentInChildren<SpriteRenderer>();
        }

        item_manager = GetComponentInParent<ItemManager>();
        if(item_manager == null)
        {
            item_manager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
        }

    }

    void OnMouseEnter()
    {
        mouse_over = true;
    }

    private void OnMouseExit()
    {
        mouse_over = false;
    }

    private void OnMouseDown()
    {
        mouse_clicked = true;
    }

    public void Deselect()
    {
        if(selected)
        {
            this.item_manager.DeselectMe(this);
        }
    }

    private void OnMouseUp()
    {
        mouse_clicked = false;
        if (!selected)
        {
            this.item_manager.SelectMe(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (mouse_over)
            sprite_renderer.color = new Color(0.0f, 1.0f, 1.0f);
        else
            sprite_renderer.color = new Color(1.0f, 1.0f, 1.0f);

        if(mouse_clicked)
        {
            sprite_renderer.color = new Color(0.0f, 0.8f, 0.8f);
        }

        if(selected)
        {
            sprite_renderer.color = new Color(0.7f, 0.3f, 0.2f);
        }
    }
}
