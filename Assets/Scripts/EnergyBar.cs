﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBar : MonoBehaviour
{
    public GameObject Head;
    public GameObject Fill;
    public float offset;
    public float energyPoints;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // position
        this.transform.position = new Vector3
            (
                this.transform.position.x,
                this.Head.transform.position.y + offset,
                this.transform.position.z
            );

        energyPoints = Mathf.Clamp(energyPoints, 1, 100);
        Fill.transform.localScale = new Vector3(energyPoints / 100, 1, 1);
    }
}
