﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreesManager : MonoBehaviour
{
    public GameObject player;
    public GameObject TreePrefab;
    public GameObject BushPrefab;
    public GameObject TurretPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlantRegularTree()
    {
        if (player.GetComponent<PlayerScript>().material_amount >= TreePrefab.GetComponent<HealthState>().health_factor)
        {
            Instantiate(TreePrefab, new Vector3(player.transform.position.x, player.transform.position.y + 2, player.transform.position.z), new Quaternion(), this.transform);
            player.GetComponent<PlayerScript>().material_amount -= TreePrefab.GetComponent<HealthState>().health_factor;
            // player.GetComponent<PlayerScript>().robotEnergy -= TreePrefab.GetComponent<HealthState>().health_factor;
        }
    }


    public void PlantTurret()
    {
        if (player.GetComponent<PlayerScript>().material_amount >= TreePrefab.GetComponent<HealthState>().health_factor)
        {
            Instantiate(TurretPrefab, new Vector3(player.transform.position.x, player.transform.position.y + 2, player.transform.position.z), new Quaternion(), this.transform);
            player.GetComponent<PlayerScript>().material_amount -= TurretPrefab.GetComponent<HealthState>().health_factor;
        }
    }

    public void PlantBush()
    {
        if (player.GetComponent<PlayerScript>().material_amount >= BushPrefab.GetComponent<HealthState>().health_factor)
        {
            Instantiate(BushPrefab, new Vector3(player.transform.position.x, player.transform.position.y + 2, player.transform.position.z), new Quaternion(), this.transform);
            player.GetComponent<PlayerScript>().material_amount -= BushPrefab.GetComponent<HealthState>().health_factor;
        }
    }
}
