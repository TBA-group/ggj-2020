﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPositioning : MonoBehaviour
{
    BoxCollider2D boxCollider;
    public float offset = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        boxCollider = this.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame

    // makes the object be above the other object or below the other object
    void Update()
    {
        float height = boxCollider.size.y;
        float y_coordinate = boxCollider.transform.position.y;


        SpriteRenderer[] sprite_renderers = GetComponentsInChildren<SpriteRenderer>();

        float max_y = 0;

        for (int i = 0; i < sprite_renderers.Length; i++)
        {
            float curr_y = sprite_renderers[i].bounds.extents.y;

            if (curr_y > max_y)
                max_y = curr_y;

        }

        SpriteRenderer[] sprite_renderers_in_myself = GetComponents<SpriteRenderer>();

        for (int i = 0; i < sprite_renderers_in_myself.Length; i++)
        {
            float curr_y = sprite_renderers_in_myself[i].bounds.extents.y;

            if (curr_y > max_y)
                max_y = curr_y;
        }

        int sorting_level = Mathf.RoundToInt((this.transform.position.y - max_y) * 100f) * -1;


        for (int i = 0; i < sprite_renderers.Length; i++)
            sprite_renderers[i].sortingOrder = sorting_level;


        for (int i = 0; i < sprite_renderers_in_myself.Length; i++)
        {
            sprite_renderers_in_myself[i].sortingOrder = sorting_level;
        }


        //this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y,
        //    (this.transform.position.y - max_y) * 0.01f);
        

    }
}
