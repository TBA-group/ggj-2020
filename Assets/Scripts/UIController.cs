﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIController : MonoBehaviour
{
    public GameObject panel_action;
    

    bool panel_invoked = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    public bool IsPointerOverUIElement()
    {
        var eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);
        if (results.Count > 0)
        {
            return results[0].gameObject.tag.Equals("Menu");
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsPointerOverUIElement())
        {
            panel_action.GetComponent<Animator>().Play("SlideIN");
            panel_invoked = true;

        }
        else
        {
            panel_action.GetComponent<Animator>().Play("SlideOUT");
            panel_invoked = false;
        }
    }
}
