﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManagerScript : MonoBehaviour
{

    public GameObject EnemyPrefab;
    public float last_spawned = 0;
    public float time_wait = 12;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        time_wait *= 0.99999999f;
        time_wait = Mathf.Clamp(time_wait, 7, 15);


        float t = Time.time;
        
        if (t - last_spawned > time_wait)
        {
            Vector3 pos = GameObject.Find("Player").transform.position;
            Instantiate(EnemyPrefab, new Vector3(pos.x + Random.value * 20 - 10f, pos.y + Random.value * 20 - 10f, 1), new Quaternion(), this.transform);
            last_spawned = Time.time;
        }
    }
}
