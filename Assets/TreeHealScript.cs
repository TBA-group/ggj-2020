﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeHealScript : MonoBehaviour
{
    public float heal_sum = 1;
    public float heal_radius = 20;
    public Material mat;

    List<GameObject> to_heal = new List<GameObject>();
    List<LineRenderer> line_renderers = new List<LineRenderer>();
    private LineRenderer line;

    // Start is called before the first frame update
    void Start()
    {
        line = this.gameObject.AddComponent<LineRenderer>();
        line.SetWidth(0.5F, 0.5F);
        // Set the number of vertex fo the Line Renderer
        line.SetVertexCount(2);
        line.startColor = Color.yellow;
        line.endColor = Color.yellow;
    }

    // Update is called once per frame
    void Update()
    {

        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, heal_radius/2);
        float min_distance = 1000f;
        GameObject chosen = null;


        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].gameObject.name == "Player")
            {
                bool flag = false;
                foreach(var x in to_heal)
                {
                    if (x.name == hitColliders[i].gameObject.name)
                    {
                        flag = true;
                    }
                }
                if (!flag)
                {
                    LineRenderer lr = new GameObject().AddComponent<LineRenderer>();
                    lr.transform.SetParent(transform, false);
                    // just to be sure reset position and rotation as well
                    lr.material = mat;

                    lr.gameObject.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
                    to_heal.Add(hitColliders[i].gameObject);

                    lr.SetWidth(0.2F, 0.2F);
                    // Set the number of vertex fo the Line Renderer
                    lr.SetVertexCount(2);

                    line_renderers.Add(lr);
                }
            }
        }

        for (int i = 0; i < to_heal.Count; i++)
        {
            GameObject to_line = to_heal[i];
            LineRenderer line = line_renderers[i];
            
            line.SetPosition(0, this.transform.position);
            line.SetPosition(1, to_line.transform.position);
            if(to_line.name == "Player")
            {
                to_line.GetComponent<PlayerScript>().robotEnergy += 0.5f;

            }
            float dist = Vector3.Distance(this.transform.position, to_line.transform.position);
            if (dist > heal_radius)
            {

                Debug.Log("Why");
                to_heal.Remove(to_line);
                line_renderers.Remove(line);
                Destroy(line.gameObject);
            }

        }

    }

}
